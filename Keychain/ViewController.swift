import UIKit
import KeychainAccess
class ViewController: UIViewController {

  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var nameTextfield: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    updateName()
  }
  
  @IBAction func saveNameButtonTouch(_ sender: Any) {
    do {
      try Keychain().set(nameTextfield.text ?? "a", key: "firstName")
    } catch let error {
      print("error: \(error)")
    }
    updateName()
  }
  
  fileprivate func updateName() {
    do {
      try nameLabel.text = Keychain().get("firstName")
    } catch let error {
      print("error: \(error)")
    } 
  }
}

